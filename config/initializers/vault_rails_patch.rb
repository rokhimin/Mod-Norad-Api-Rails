# frozen_string_literal: true

module Vault
  module Rails
    class << self
      def memory_key_for(path, key)
        # AES 128 requires a key length of 16
        Base64.strict_encode64("#{path}/#{key}".ljust(16, 'x')).byteslice(0..15)
      end
    end
  end
end
