# frozen_string_literal: true

module NoradRelayHelpers
  def relay_opts(organization)
    relay_queue = organization.docker_relay_queue
    if relay_queue
      {
        relay_secret: organization.relay_encryption_key,
        relay_queue: relay_queue,
        relay_exchange: organization.token
      }
    else
      {}
    end.with_indifferent_access
  end
end
