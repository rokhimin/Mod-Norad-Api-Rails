# frozen_string_literal: true

module NoradScanBuilder
  class HostScanArgs < ScanArgs
    attr_reader :machine

    def initialize(container, opts = {})
      @machine = opts.fetch(:machine)
      super
    end

    private

    def args_array
      cargs = container_config(machine) || {}
      create_command_array({ target: machine.target_address }.merge(cargs))
    end
  end
end
