# frozen_string_literal: true

module IpUtils
  def self.rfc1918_relay_required?
    ENV['RFC1918_RELAY_OPTIONAL'] != 'true'
  end
end
